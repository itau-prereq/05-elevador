package br.com.itau;

import java.util.List;

public class Elevador {
    private List<Passageiro> passageiros;
    private int andar;
    private int capacidade;
    private int numeroAndares;

    public Elevador(List<Passageiro> passageiros, int andar, int capacidade, int numeroAndares) {
        this.passageiros = passageiros;
        this.andar = andar;
        this.capacidade = capacidade;
        this.numeroAndares = numeroAndares;
    }

    public void sobe() throws Exception{
        if (this.andar == this.numeroAndares){
            throw new Exception("Estamos no topo");
        }
        else{
            this.andar += 1;
        }
    }

    public void desce() throws Exception{
        if (this.andar == 0){
            throw new Exception("Estamos no terreo");
        }
        else{
            this.andar -= 1;
        }
    }

    public void embarcaPassageiro(Passageiro passageiro) throws Exception{
        if (this.passageiros.size() == this.getCapacidade()){
            throw new Exception("Capacidade maxima atingida");
        }
        else{
            this.passageiros.add(passageiro);
        }
    }

    public void desembarcaPassageiro(Passageiro passageiro) throws Exception{
        if (this.passageiros.size() == 0){
            throw new Exception("Nao ha passageiros para desembarcar");
        }
        else{
            this.passageiros.remove(0);
        }
    }

    public List<Passageiro> getPassageiros() {
        return passageiros;
    }

    public void setPassageiros(List<Passageiro> passageiros) {
        this.passageiros = passageiros;
    }

    public int getAndar() {
        return andar;
    }

    public void setAndar(int andar) {
        this.andar = andar;
    }

    public int getCapacidade() {
        return capacidade;
    }

    public void setCapacidade(int capacidade) {
        this.capacidade = capacidade;
    }

    public int getNumeroAndares() {
        return numeroAndares;
    }

    public void setNumeroAndares(int numeroAndares) {
        this.numeroAndares = numeroAndares;
    }
}
