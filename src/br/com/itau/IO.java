package br.com.itau;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class IO {
    public static void imprimeMsgInicial(){
        System.out.println("Bem vindo ao elevador");
    }

    public static String getAcao(Elevador elevador){
        Scanner scanner = new Scanner(System.in);

        imprimeElevador(elevador);
        System.out.println("O que deseja fazer? (subir/descer ou embarcar/desembarcar ou sair) ");

        return scanner.nextLine();
    }

    public static void imprimeElevador(Elevador elevador){
        String andar = new String();
        if(elevador.getAndar() != 0){
            andar = elevador.getAndar() + " andar";
        } else {
            andar = "terreo";
        }

        System.out.println("Andar: " + andar + " de " + elevador.getNumeroAndares() +
                           " andares\nPassageiros: " + elevador.getPassageiros().size() + "/" + elevador.getCapacidade());
    }

    //DTO - data transfer object
    public static Map<String, Integer> obterElevador(){
        Scanner scanner = new Scanner(System.in);

        Map<String, Integer> elevador = new HashMap<>();

        System.out.println("Numeros de andares: ");
        int andares = scanner.nextInt();
        System.out.println("Capacidade: ");
        int capacidade = scanner.nextInt();

        elevador.put("andares", andares);
        elevador.put("capacidade", capacidade);

        return elevador;
    }

    public static void imprimeErro(String msg){
        System.out.println(msg);
    }

    public String continuar() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Jogar novamente? (s/n) ");

        return scanner.nextLine();
    }

}
