package br.com.itau;

public class Passageiro {
    private int andarDesejado;
    private int andarAtual;

    public int getAndarAtual() {
        return andarAtual;
    }

    public void setAndarAtual(int andarAtual) {
        this.andarAtual = andarAtual;
    }

    public int getAndarDesejado() {
        return andarDesejado;
    }

    public void setAndarDesejado(int andarDesejado) {
        this.andarDesejado = andarDesejado;
    }
}
