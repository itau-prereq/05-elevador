package br.com.itau;

import java.util.ArrayList;
import java.util.Map;

public class Main {

    public static void main(String[] args) {
	// write your code here
        IO io = new IO();
        io.imprimeMsgInicial();

        Map<String, Integer> entradaElevador = io.obterElevador();
        Elevador elevador = new Elevador(new ArrayList<Passageiro>(), 0, entradaElevador.get("capacidade"), entradaElevador.get("andares"));

        boolean fim = false;
        while(!fim) {
            String acao = io.getAcao(elevador);

            if(acao.equals("subir")){
                try{
                    elevador.sobe();
                } catch (Exception e){
                    io.imprimeErro(e.getMessage());
                }
            } else
            if(acao.equals("descer")){
                try{
                    elevador.desce();
                } catch (Exception e){
                    io.imprimeErro(e.getMessage());
                }
            }else
            if(acao.equals("embarcar")){
                try{
                    elevador.embarcaPassageiro(new Passageiro());
                } catch (Exception e){
                    io.imprimeErro(e.getMessage());
                }
            }else
            if(acao.equals("desembarcar")){
                try{
                    elevador.desembarcaPassageiro(new Passageiro());
                } catch (Exception e){
                    io.imprimeErro(e.getMessage());
                }
            }else
            if(acao.equals("sair")){
                fim = true;
            }

        }


    }
}
